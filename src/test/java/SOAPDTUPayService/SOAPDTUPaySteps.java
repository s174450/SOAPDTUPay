package SOAPDTUPayService;

import dtu.ws.fastmoney.*;
import io.cucumber.java.PendingException;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.math.BigDecimal;

public class SOAPDTUPaySteps {

    BankService bank = new BankServiceService().getBankServicePort();
    String costumerAccount;
    String merchantAccount;
    User costumerUser = new User();
    User merchantUser = new User();

    @Given("the customer {string} {string} with CPR {string} has a bank account")
    public void the_customer_with_cpr_has_a_bank_account(String string, String string2, String string3) throws BankServiceException_Exception {
        costumerUser.setFirstName(string);
        costumerUser.setLastName(string2);
        costumerUser.setCprNumber(string3);
    }

    @Given("the balance of the costumer account is {int}")
    public void the_balance_of_the_costumer_account_is(Integer int1) throws BankServiceException_Exception {
        System.out.println(costumerAccount);
        bank.retireAccount(bank.getAccount(costumerUser.getCprNumber()).getId());
        costumerAccount = bank.createAccountWithBalance(costumerUser,BigDecimal.valueOf(int1));
        System.out.println(costumerAccount);
    }

    @Given("the customer is registered with DTUPay")
    public void the_customer_is_registered_with_dtu_pay() {
        // We have to define, based on implementation registration context. Object()
        // If a costumer merchant is emitting a transfer from DTUPay the
        throw new PendingException();
    }

    @Given("the merchant {string} {string} with CPR number {string} has a bank account")
    public void the_merchant_with_cpr_number_has_a_bank_account(String string, String string2, String string3) throws BankServiceException_Exception {
        merchantUser.setFirstName(string);
        merchantUser.setLastName(string2);
        merchantUser.setCprNumber(string3);
    }

    @Given("the balance of the merchant account is {int}")
    public void the_balance_of_the_merchant_account_is(Integer int1) throws BankServiceException_Exception {
        merchantAccount = bank.createAccountWithBalance(merchantUser,BigDecimal.valueOf(int1));
    }

    @Given("the merchant is registered with DTUPay")
    public void the_merchant_is_registered_with_dtu_pay() {
        throw new PendingException();
    }

    @When("the merchant initiates a payment for {string} kr by the customer")
    public void the_merchant_initiates_a_payment_for_kr_by_the_customer(String string) throws BankServiceException_Exception {
        bank.transferMoneyFromTo(costumerAccount, merchantAccount, new BigDecimal(string), "Money transfer");
    }

    @Then("the payment is successful")
    public void the_payment_is_successful(){
        // TODO: How should w
    }

    @Then("the balance of the customer at the bank is {int} kr")
    public void the_balance_of_the_customer_at_the_bank_is_kr(Integer int1) throws BankServiceException_Exception {
        BigDecimal amount = bank.getAccount(costumerUser.getCprNumber()).getBalance();
        Assert.assertEquals(amount, new BigDecimal(990));
    }

    @Then("the balance of the merchant at the bank is {int} kr")
    public void the_balance_of_the_merchant_at_the_bank_is_kr(Integer int1) throws BankServiceException_Exception {
        BigDecimal amount = bank.getAccount(merchantUser.getCprNumber()).getBalance();
        Assert.assertEquals(amount, new BigDecimal(2010));
    }

    @After
    public void clean() throws BankServiceException_Exception {
        bank.retireAccount(bank.getAccount(merchantUser.getCprNumber()).getId());
        bank.retireAccount(bank.getAccount(costumerUser.getCprNumber()).getId());
    }
}
